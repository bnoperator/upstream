library(upstream)
library(bnutil)
library(dplyr)
library(reshape2)
library(data.table)
library(pgFCS)
library(foreach)
library(doParallel)

getData = function() {
  do2g = FALSE
  if(do2g){
    df = pData(aData)
    mf = varMetadata(aData)
    bGrp = mf[["labelDescription"]] == "Pazopanib"
    mf$groupingType = as.character(mf$groupingType)
    mf$groupingType[bGrp] = "Color"
  } else {
    load("slope_data.RData")
    df = slope_data$data
    mf = slope_data$metadata
  }
  return(AnnotatedData$new(data=df, metadata=mf))
}

aData = getData()
df = aData$data
aResult = scanAnalysis0(df, dbFrame = upstream::UpstreamDatabase)


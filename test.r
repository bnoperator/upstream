makePerPeptidePlot = function(df, dbFrame, scanRank, minPScore){
  dbFrame= subset(dbFrame, Database != "phosphoNET" |  Kinase_PredictorVersion2Score > minPScore)
  dbFrame = subset(dbFrame, Kinase_rank <= scanRank)
  ixList = intersectById(dbFrame, df)
  dbFrame = ixList[[1]]
  df = ixList[[2]]

  dbFrame = dbFrame %>% group_by(ID) %>% summarise(lowestRank = min(Kinase_rank))


  if (!is.null(df[["grp"]])){
    perPepStats = df %>% group_by(ID) %>% do({
      thisPep = subset(dbFrame, ID == .$ID)
      print(thisPep)
      aTest = t.test(value ~ grp, data = ., var.equal = TRUE)
      aResult = data.frame(pes = -diff(aTest$estimate),
                           cil = aTest$conf.int[1],
                           ciu = aTest$conf.int[2],
                           lowRank = thisPep$lowestRank
      )

    })
    ppp = ggplot(perPepStats, aes(x = reorder(ID, lowRank), colour = as.factor(lowRank), y = pes, ymin = cil, ymax = ciu)) + geom_point() + geom_errorbar()
    ppp = ppp + coord_flip()
    ppp = ppp + xlab("Peptide ID") + ylab("Group difference")
    return(ppp)
  } else {
    return(NULL)
  }
}

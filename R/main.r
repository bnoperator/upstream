#' @import dplyr bnutil plyr
#' @export
shinyServerRun = function(input, output, session, context) {
  getFolderReactive = context$getRunFolder()
  getDataReactive = context$getData()
  output$body = renderUI({
        sidebarLayout(sidebarPanel(
                          tags$div(HTML("<strong><font color = green>Upstream Kinase Analysis 86312</font></strong>")),
                          tags$hr(),
                          actionButton("start", "Start"),
                          tags$hr(),
                          verbatimTextOutput("status")

                      ),
                      mainPanel(
                        tabsetPanel(
                          tabPanel("Basic Settings",
                                   tags$hr(),
                                   sliderInput("scan", "Scan Rank From-To", min = 1, max = 20, value = c(4,12),round = TRUE),
                                   sliderInput("nperms", "Number of permutations", min = 100, max = 1000, value = 500, step = 100, round = TRUE)
                                 ),

                          tabPanel("Advanced Settings"                                   ,
                                   tags$hr(),
                                   sliderInput("wHprd", "HPRD weight", min = 0, max = 1, value = 1),
                                   sliderInput("wPhosphoSite", "PhosphoSite weight", min = 0, max = 1, value = 1),
                                   sliderInput("wReactome", "Reactome weight", min = 0, max = 1, value = 1),
                                   sliderInput("wPhosphoNET", "PhosphoNET weight", min = 0, max = 1, value = 1),
                                   tags$hr(),
                                   numericInput("minPScore", "Minimal PhosphoNET prediction score", value = 300),
                                   tags$hr())
                        )
                      )
        )
  })

  getGroupingLabel = function(aData){
    if (length(aData$colorLabels) > 1){
      stop("Need at most one Color to define the grouping")
    }
    else if(length(aData$colorLabels) == 1){
      groupingLabel = aData$colorColumnNames
      grp = as.factor((aData$data[[groupingLabel]]))
      if (length( levels(grp) ) != 2){
        stop(paste("Wrong number of groups, found:", levels(grp)))
      }

      df = subset(aData$data, rowSeq = 1)
      grp = as.factor((df[[groupingLabel]]))
      if (min(summary(grp)) < 2){
        stop("Need at least 2 observations per group.")
      }
      return(groupingLabel)
    }
    else {
      if(max(aData$data$colSeq)> 1){
        stop(paste("Can not run this data without a grouping factor.", aData$colorLabels))
      } else return(NULL)
    }
  }

  nid = showNotification("Press Start to start the analysis.", duration = NULL, type = "message", closeButton = FALSE)

  observe({
    getData=getDataReactive$value
    getFolder = getFolderReactive$value
    if (is.null(getData)) return()
    adf = getData()
    output$status = renderText({
      grp = getGroupingLabel(adf)
      if (input$start == 0){
        if (!is.null(grp)){
          return(paste("Grouping factor:", grp))
        } else {
          return("Grouping factor: none")
        }
      }
      df = adf$data
      db = upstream2::UpstreamDatabase
      nCores = detectCores()
      msg = paste("Please wait ... running analysis. Using", nCores, "cores.")
      showNotification(ui = msg, id = nid, type = "message", closeButton = FALSE, duration = NULL)
      if(!is.null(grp)){
        df$grp = df[[grp]]
        aResult = scanAnalysis2g(df,  dbFrame = db,
                                  scanRank = input$scan[1]:input$scan[2],
                                  nPermutations = input$nperms,
                                  dbWeights = c(HPRD = input$wHprd,
                                                PhosphoNET = input$wPhosphoNET,
                                                Phosphosite = input$wPhosphoSite,
                                                Reactome = input$wReactome
                                                ),
                                  minPScore = input$minPScore)
      } else {
        aResult = scanAnalysis0(df,  dbFrame = db,
                                 scanRank = input$scan[1]:input$scan[2],
                                 nPermutations = input$nperms,
                                 dbWeights = c(HPRD = input$wHprd,
                                               PhosphoNET = input$wPhosphoNET,
                                               Phosphosite = input$wPhosphoSite,
                                               Reactome = input$wReactome
                                 ),
                                 minPScore = input$minPScore)
      }
      showNotification(ui = "Done", id = nid, type = "message", closeButton = FALSE)
      aFull = ldply(aResult, .fun = function(.)return(data.frame(.$aResult, mxRank = .$mxRank) ))


      settings = data.frame(setting = c("ScanRank Min", "ScanRank Max", "Number of Permutations", "HPRD weight", "Phosphosite weight", "Reactome weight", "PhosphoNET weight", "Min PhosphoNet score"),
                            value   = c(input$scan[1] , input$scan[2], input$nperms, input$wHprd, input$wPhosphoSite, input$wReactome, input$wPhosphoNET, input$minPScore) )

      spath = file.path(getFolder(), "runData.RData")
      save(file = spath, df, aResult, aFull, settings)
      out = data.frame(rowSeq = 1, colSeq = 1, dummy = NaN)
      meta = data.frame(labelDescription = c("rowSeq", "colSeq", "dummy"), groupingType = c("rowSeq", "colSeq", "QuantitationType"))
      result = AnnotatedData$new(data = out, metadata = meta)
      context$setResult(result)
      return("Done")
    })

  })
}

#' @export
shinyServerShowResults = function(input, output, session, context){
  getFolderReactive = context$getRunFolder()
  output$body = renderUI(
    sidebarLayout(
      sidebarPanel(
        tags$div(HTML("<strong><font color = green>Upstream Kinase Analysis 2.0</font></strong>")),
        tags$hr(),
        textOutput("grpText"),
        tags$hr(),
        sliderInput("minsetsize", "Include results based on peptide sets with minimal size of:",
                    min = 1,
                    max = 5,
                    step = 1,
                    value = 3),
        width = 3),
      mainPanel(
        tabsetPanel(
          tabPanel("Upstream Kinase Score",
                   helpText("This is a score plot"),
                   selectInput("spsort", "Sort score plot on", choices = list(Score = "score", Statistic = "stat")),
                   actionLink("saveScorePlot", "Save score plot"),
                   plotOutput("scorePlot", height = "1400px")
          ),
          tabPanel("Kinase Volcano",
                   helpText("This is a volcanoplot"),
                   plotOutput("volcanoPlot", height = "800px")
          ),
          tabPanel("Kinase Details",
                   helpText("These are kinase details"),
                   selectInput("showKinase", "Select kinase", choices = ""),
                   tags$hr(),
                   actionLink("uniprot", "..."),
                   tags$hr(),
                   tableOutput("kinaseSummary"),
                   tabsetPanel(
                     tabPanel("Details Table",
                              helpText(""),
                              actionLink("saveDetailsTable", "Save details table"),
                              tableOutput("kinaseDetails")
                     ),
                     tabPanel("Per peptide plot",
                              helpText(""),
                              actionLink("savePerpeptidePlot", "Save per peptide plot"),
                              plotOutput("perPeptidePlot", height = "800px")

                     )
                   )
          ),
          tabPanel("Report",
                   helpText("The table below shows the settings that were used for this analysis."),
                   tableOutput("InfoSettings"),
                   helpText("The below shows the summary results of the analysis"),
                   actionLink("saveSummaryResults", "Save summary results"),
                   tableOutput("SummaryTable")
          )
        )
      )

    )
  )

  observe({
    getFolder = getFolderReactive$value
    if(is.null(getFolder)) return()
    spath = file.path(getFolder(), "runData.RData")
    load(file = spath)

    kinase2uniprot = upstream2::UpstreamDatabase%>%group_by(Kinase.UniprotID)%>% dplyr::summarise(Kinase.Name = Kinase.Name[1])

    output$grpText = renderText({
        grp = df[["grp"]]
        if (!is.null(grp)){
          txt = paste("Grouping factor with levels", levels(grp)[1], "and", levels(grp)[2])
        } else {
          return("Grouping factor: none")
        }
    })

    xaxText = function(){
      grp = df[["grp"]]
      if (!is.null(grp)){
        txt = paste(">0 indicates higher activity in the",levels(grp)[2], "group")
      } else {
        return(NULL)
      }
    }

    scorePlot = reactive({
      aSub = subset(aFull, nFeatures >= input$minsetsize)
      cs = makeScorePlot(aSub, input$spsort)
      xax = paste("Normalized kinase statistic (", xaxText(),")", sep = "")
      cs = cs + ylab(xax)
    })

    output$scorePlot = renderPlot({
      print(scorePlot())
    })

    aSummary = reactive({
      aSub = subset(aFull, nFeatures >= input$minsetsize)
      aSum = makeSummary(aSub)
      updateSelectInput(session, "showKinase", choices = aSum$ClassName)
      aSum
    })

    output$volcanoPlot = renderPlot({
      vp = makeVolcanoPlot(aSummary())
      print(vp)
    })

    output$perPeptidePlot = renderPlot({
      db = subset(upstream2::UpstreamDatabase , Kinase.Name == input$showKinase)
      aPlot = makePerPeptidePlot(df,
                                 db,
                                scanRank = subset(settings, setting == "ScanRank Max")$value,
                                minPScore = subset(settings,setting == "Min PhosphoNet score")$value)
      print(aPlot)
    })

    output$kinaseSummary = renderTable({
      aSum = aSummary()
      aKin = subset(aSum, ClassName == input$showKinase)
      aTable = data.frame(name  = c("Mean Kinase Statistic", "Mean Specificity Score", "Mean Significance Score", "Median Combined Score"),
                          value = c(aKin$meanStat, aKin$meanFeatScore, aKin$meanPhenoScore, aKin$medianScore))
      colnames(aTable) = c(input$showKinase, "value")
      return(aTable)
    })

    observeEvent(input$showKinase, {
      upid = kinase2uniprot %>% filter(Kinase.Name == input$showKinase)%>%.$Kinase.UniprotID
      aLabel = paste(input$showKinase," (",upid,") on Uniprot.org", sep = "")
      updateActionButton(session, "uniprot", label = aLabel)
    })


    detailsTable = reactive({
      aTable = makeDetailsTable(df,
                                subset(upstream2::UpstreamDatabase , Kinase.Name == input$showKinase),
                                scanRank = subset(settings, setting == "ScanRank Max")$value,
                                minPScore = subset(settings,setting == "Min PhosphoNet score")$value)
    })

    output$kinaseDetails = renderTable({
        aTable = detailsTable()
    })

    output$InfoSettings = renderTable({
      getSettingsInfo(settings)
    })

    observeEvent(input$saveDetailsTable, {
      aTable = detailsTable()
      filename = file.path(getFolder(), paste(gsub("/", "-",input$showKinase), format(Sys.time(), "%Y%m%d-%H%M.txt")) )
      write.table(aTable, filename, sep = "\t", quote = FALSE, row.names = FALSE)
      shell.exec(getFolder())
    })

    observeEvent(input$saveScorePlot, {
      filename = file.path(getFolder(), paste("UpstreamScorePlot", format(Sys.time(), "%Y%m%d-%H%M.png")) )
      ggsave(filename, scorePlot(), device = "png" ,units = "cm", height = 40, width = 28)
      shell.exec(getFolder())
    })

    observeEvent(input$uniprot, {
      upid = kinase2uniprot %>% filter(Kinase.Name == input$showKinase)%>%.$Kinase.UniprotID
      browseURL(paste("http://www.uniprot.org/uniprot/", upid, sep = ""))
    })

    summaryResultTable = reactive({
      df = aSummary()
      df = left_join(kinase2uniprot, df, by = c("Kinase.Name" = "ClassName"))
      df = df%>% filter(!is.na(medianScore))%>%arrange(-medianScore)
    })

    observeEvent(input$saveSummaryResults, {
      aTable = summaryResultTable()
      filename = file.path(getFolder(), paste("Summaryresults", format(Sys.time(), "%Y%m%d-%H%M.txt")) )
      write.table(aTable, filename, sep = "\t", quote = FALSE, row.names = FALSE)
      shell.exec(getFolder())
    })

    output$SummaryTable = renderTable({
      summaryResultTable()
    })

  })
}

# README #
This package is obsolete!

Bionavigator RStepOperator for Upstream Kinase Analysis

# Preparation of the upstream db (log) #
bLit = with(aCube,Database != "PhosphoNET")
aCube$Kinase_PredictorVersion2Score[bLit] = 1
aCube$Kinase_rank[bLit] = 0
aCube = aCube %>% group_by(Database) %>%
dplyr::mutate(., Relative_Kinase_Score = Kinase_PredictorVersion2Score/max(Kinase_PredictorVersion2Score))

